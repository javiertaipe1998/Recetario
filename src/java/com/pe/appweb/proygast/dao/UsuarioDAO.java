/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pe.appweb.proygast.dao;
import com.pe.appweb.proygast.entidades.Usuario;
import com.pe.appweb.proygast.service.UsuarioService;
import com.pe.appweb.proygast.utils.HibernateUtil;
import java.math.BigDecimal;
import java.util.List;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

/**
 *
 * @author Carlos
 */
public class UsuarioDAO implements UsuarioService{
    private Session session;
    @Override
    public boolean ActualizarUsuario(Session session,Usuario usu) {
        boolean resp = false;
        try {
            session.update(usu);
            resp = true;
        } catch (Exception e) {
            resp = false;
        }
        
        return resp;
    }

    @Override
    public List<Usuario> ListadoUsuarioTodos(Session session) throws Exception{
        //System.out.println("ENTRO AL DAO!!");
        String hql = " from Usuario";        
        Query query = session.createQuery(hql);
        List<Usuario> listaTCP= (List<Usuario>) query.list();
        //System.out.println("Total es: "+listaTCP.size());
        return listaTCP;
//        return session.createCriteria(Mempresa.class).list();
    }
    

    @Override
    public Usuario ListadoUsuarioxId(Session session, int IdTurno) {
        return (Usuario) session.get(Usuario.class, IdTurno);
    }

    @Override
    public boolean GrabarUsuario(Usuario usu) {
        boolean resp = false;
        session=HibernateUtil.getSessionFactory().openSession();
        Transaction transaction=session.beginTransaction();
        try {
            session.save(usu);
            transaction.commit();
            resp = true;
        } catch (Exception e) {
            session.close();
            resp = false;
        }
        
        return resp;
    }
    
    
    @Override
    public Integer ContadorDeRegUsuario(Session session) throws Exception{
        String hql = "select count(*) from Usuario";
        Query query = session.createQuery(hql);
        Long FilasTab=(Long) query.uniqueResult();
        Integer cont=FilasTab.intValue();
        return cont;
    }
    
    @Override
    public Usuario validarUsuxVarnomusu(Session session, String nomusu){
            String hql="from Usuario where VARNOMUSUARIO =:VARNOMUSUARIO ";
            Query query=session.createQuery(hql);
            query.setParameter("VARNOMUSUARIO",nomusu);
            Usuario tUsuario=(Usuario) query.uniqueResult();
            //System.out.print("USUARIO"+idusuario);
            
            return tUsuario;
    }
    
    
}

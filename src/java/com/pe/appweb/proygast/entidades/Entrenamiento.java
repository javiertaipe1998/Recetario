package com.pe.appweb.proygast.entidades;
// Generated 09/11/2016 10:16:46 AM by Hibernate Tools 4.3.1


import java.util.HashSet;
import java.util.Set;

/**
 * Entrenamiento generated by hbm2java
 */
public class Entrenamiento  implements java.io.Serializable {


     private int intcodentrenamiento;
     private Estudiante estudiante;
     private int intpuntajeacum;
     private Character chrestado;
     private Set detaentrenas = new HashSet(0);

    public Entrenamiento() {
    }

	
    public Entrenamiento(int intcodentrenamiento, Estudiante estudiante, int intpuntajeacum) {
        this.intcodentrenamiento = intcodentrenamiento;
        this.estudiante = estudiante;
        this.intpuntajeacum = intpuntajeacum;
    }
    public Entrenamiento(int intcodentrenamiento, Estudiante estudiante, int intpuntajeacum, Character chrestado, Set detaentrenas) {
       this.intcodentrenamiento = intcodentrenamiento;
       this.estudiante = estudiante;
       this.intpuntajeacum = intpuntajeacum;
       this.chrestado = chrestado;
       this.detaentrenas = detaentrenas;
    }
   
    public int getIntcodentrenamiento() {
        return this.intcodentrenamiento;
    }
    
    public void setIntcodentrenamiento(int intcodentrenamiento) {
        this.intcodentrenamiento = intcodentrenamiento;
    }
    public Estudiante getEstudiante() {
        return this.estudiante;
    }
    
    public void setEstudiante(Estudiante estudiante) {
        this.estudiante = estudiante;
    }
    public int getIntpuntajeacum() {
        return this.intpuntajeacum;
    }
    
    public void setIntpuntajeacum(int intpuntajeacum) {
        this.intpuntajeacum = intpuntajeacum;
    }
    public Character getChrestado() {
        return this.chrestado;
    }
    
    public void setChrestado(Character chrestado) {
        this.chrestado = chrestado;
    }
    public Set getDetaentrenas() {
        return this.detaentrenas;
    }
    
    public void setDetaentrenas(Set detaentrenas) {
        this.detaentrenas = detaentrenas;
    }




}



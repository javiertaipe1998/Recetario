/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pe.appweb.proygast.utils;

import java.math.BigDecimal;

/**
 *
 * @author joseasto
 */
public class varGlobales {
    private static BigDecimal pubCodProfesor;
    private static BigDecimal pubCodPadre;
    private static BigDecimal pubCodAlumno;
    private static BigDecimal pubCodGrado;
    private static BigDecimal pubCodCurso;
    private static String nomusuario;
    private static String nomperfil;
    private static int pubCodPersonal;

    public BigDecimal getPubCodProfesor() {
        return pubCodProfesor;
    }

    public void setPubCodProfesor(BigDecimal pubCodProfesor) {
        varGlobales.pubCodProfesor = pubCodProfesor;
    }

    public BigDecimal getPubCodPadre() {
        return pubCodPadre;
    }

    public void setPubCodPadre(BigDecimal pubCodPadre) {
        varGlobales.pubCodPadre = pubCodPadre;
    }

    public BigDecimal getPubCodAlumno() {
        return pubCodAlumno;
    }

    public void setPubCodAlumno(BigDecimal pubCodAlumno) {
        varGlobales.pubCodAlumno = pubCodAlumno;
    }

    public BigDecimal getPubCodGrado() {
        return pubCodGrado;
    }

    public void setPubCodGrado(BigDecimal pubCodGrado) {
        varGlobales.pubCodGrado = pubCodGrado;
    }

    public BigDecimal getPubCodCurso() {
        return pubCodCurso;
    }

    public void setPubCodCurso(BigDecimal pubCodCurso) {
        varGlobales.pubCodCurso = pubCodCurso;
    }
    
    //USADAS

    public String getNomusuario() {
        return nomusuario;
    }

    public void setNomusuario(String nomusuario) {
        varGlobales.nomusuario = nomusuario;
    }

    public String getNomperfil() {
        return nomperfil;
    }

    public void setNomperfil(String nomperfil) {
        varGlobales.nomperfil = nomperfil;
    }

    public int getPubCodPersonal() {
        return pubCodPersonal;
    }

    public void setPubCodPersonal(int pubCodPersonal) {
        varGlobales.pubCodPersonal = pubCodPersonal;
    }

    

    
    
    


    

    

    
    

}

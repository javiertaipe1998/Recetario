/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pe.appweb.proygast.service;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import com.pe.appweb.proygast.entidades.Usuario;
import java.util.List;
import org.hibernate.Session;

/**
 *
 * @author CARLOS
 */
public interface UsuarioService {
    public abstract List<Usuario> ListadoUsuarioTodos(Session session) throws Exception;
    public abstract Usuario ListadoUsuarioxId(Session session,int IdUsu);   
    public abstract boolean GrabarUsuario(Usuario usu);
    public abstract boolean ActualizarUsuario(Session session,Usuario usu);
    
    public Integer ContadorDeRegUsuario(Session session) throws Exception;
    
    public Usuario validarUsuxVarnomusu(Session session, String nomusu);
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pe.appweb.proygast.managedbeanview;


//import com.pe.appweb.proygast.dao.PerfilusuarioDAO;
//import com.pe.appweb.proygast.dao.PersonalDAO;
import com.pe.appweb.proygast.dao.UsuarioDAO;
import com.pe.appweb.proygast.entidades.Perfilusario;
import com.pe.appweb.proygast.entidades.Profesor;
import com.pe.appweb.proygast.entidades.Usuario;
import com.pe.appweb.proygast.utils.HibernateUtil;
import com.pe.appweb.proygast.utils.MensajeSYSUtils;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import org.hibernate.Session;
import org.hibernate.Transaction;

import com.pe.appweb.proygast.utils.Encryptar;
import com.pe.appweb.proygast.utils.varGlobales;
import javax.faces.bean.SessionScoped;
import javax.servlet.http.HttpSession;

/**
 *
 * @author CARLOS
 */


@ManagedBean
//@SessionScoped
@ViewScoped
public class MantUsuarioMBR extends MensajeSYSUtils implements Serializable{     
    
    Session session;
    Transaction transaction;

   
    private UsuarioDAO usudao;
    private Usuario musu;
    private List<Usuario> listausu;
    
    private List<Usuario> listaUsuFiltrado;
    boolean chkestado;
//    private boolean insert = true;
    private Boolean insert = Boolean.TRUE;
    
    int idperfusu;
    private List<Perfilusario> listPerfusu;
    private Perfilusario perfilusudao;
    private Perfilusario mperfilu;
    
    int idperso;
    private List<Profesor> listPerso;
//    private PersonalDAO persodao;
    private Profesor mperso;
    
    String varnomusuario;
    String varclaveusuario;
    
    String varnomusuarioL;
    String varclaveusuarioL;
    
    
    private varGlobales varGlobal;
    
    private String varGNomusu;
    private String varGNomperfil;

    public Usuario getMusu() {
        return musu;
    }

    public void setMusu(Usuario musu) {
        this.musu = musu;
    }

    

    

    
    @PostConstruct
    private void init(){
        initInstancia();
        initlistDep();
    }
    
    private void initInstancia(){        
        this.musu = new Usuario();
        this.usudao = new UsuarioDAO(); 
        this.listausu = new ArrayList();
        
        chkestado = true;
//        this.perfilusudao = new PerfilusuarioDAO();
//        this.persodao = new PersonalDAO();
        
        this.mperfilu = new Perfilusario();
        this.mperso = new Profesor();
        this.varGlobal = new varGlobales();
        
        
    }
    
    private void initlistDep() {
        this.session=null;
        this.transaction=null;
        try {
            this.session = HibernateUtil.getSessionFactory().openSession();
            this.transaction = session.beginTransaction();  
            
//            this.listPerfusu= perfilusudao.ListadoPerfusuAct(this.session);
//            this.listPerso= persodao.ListadoPersonalAct(this.session);
            
            
            this.varGNomusu = new varGlobales().getNomusuario();
            this.varGNomperfil = new varGlobales().getNomperfil();
        
        
        } catch (Exception e) {
            System.out.println("ERROR :"+e.getMessage());
            if (this.transaction!=null){
                
                transaction.rollback();
            }
        }finally{
            if (this.session!=null){
                this.session.close();
            }
        }
        
    }

    public String registrarUsu(){
            
        this.session=null;
        this.transaction=null;
        
        try {
            this.session = HibernateUtil.getSessionFactory().openSession();
            this.transaction = this.session.beginTransaction();
            boolean respuesta;
        
            int countReg=usudao.ContadorDeRegUsuario(this.session);
            int idUsu=0;
            if(countReg!=0){
                idUsu=countReg+1;
            }else{
                idUsu=1;
            }
            this.musu = new Usuario();
            this.musu.setIntcodusuario(idUsu);
            
            this.musu.setVarnomusuario(varnomusuario);
            String claveencrip=Encryptar.sha5256(varclaveusuario);
            this.musu.setVarclaveusuario(claveencrip);
            
            this.mperfilu.setIntcodperfilusu(idperfusu);
            this.musu.setPerfilusario(this.mperfilu);
            
            this.mperso.setIntcodprofesor(idperso);
            this.musu.setProfesor(this.mperso);
            
            
            if (chkestado==true) {                
                this.musu.setChrestado('A');
            }else{                
                this.musu.setChrestado('I');
            }
       
            respuesta = usudao.GrabarUsuario(this.musu);
            this.transaction.commit();
        
        if (respuesta){
            messageInfo("Se realizo la creación del Usuario");
        }else{
            messageError("NO Se realizo la creación del Usuario");
        }
        }
        catch (Exception ex) {
            
            if (this.transaction!=null){
                this.transaction.rollback();
            }
            messageFatal("Error Fatal: Por favor contacte con su administrador"+ex.getMessage());
            
            return null;
        }
            finally
            {
                if (this.session!=null){
                    this.session.close();
                }
            }
        return "/MANTENIMIENTO/FrmManttoUsuario";
        
    }
    
    public String limpiarcajas(){
        return "/MANTENIMIENTO/FrmManttoUsuario";
    }
    
    public String limpiarcajasL(){
        return "/index";
    }
    
    public List<Usuario> listadoUsuario()
    {
        //System.out.println("INGRESO A LA FUNCION");
        this.session=null;
        this.transaction=null;
        
        try {
            this.session = HibernateUtil.getSessionFactory().openSession();
            this.transaction = this.session.beginTransaction();
            
            this.listausu= usudao.ListadoUsuarioTodos(this.session);

            this.transaction.commit();
            
            return this.listausu;
//            System.out.println(listaTEmpresa.size());
        }
        catch (Exception ex) {
            System.out.println("ERROR :"+ex.getMessage());
            if (this.transaction!=null){
                this.transaction.rollback();
            }
            messageFatal("Error Fatal: Por favor contacte con su administrador"+ex.getMessage());
            
            return null;
        }
            finally
            {
                if (this.session!=null){
                    this.session.close();
                }
            }
            
    }
    
    public String update()
    {
        this.session=null;
        this.transaction=null;
        
        try {
            
            this.session = HibernateUtil.getSessionFactory().openSession();
            this.transaction = this.session.beginTransaction();
            
            
            this.musu.setVarnomusuario(varnomusuario);
            String claveencrip=Encryptar.sha5256(varclaveusuario);
            this.musu.setVarclaveusuario(claveencrip);
            
            this.mperfilu.setIntcodperfilusu(idperfusu);
            this.musu.setPerfilusario(this.mperfilu);
            
            this.mperso.setIntcodprofesor(idperso);
            this.musu.setProfesor(this.mperso);
            
            if (chkestado==true) {                
                this.musu.setChrestado('A');
            }else{                
                this.musu.setChrestado('I');
            }
            
            usudao.ActualizarUsuario(this.session,this.musu);

            this.transaction.commit();
            
            messageInfo("Correcto: Los cambios fueron guardados correctamente");
            
            insert = Boolean.TRUE;

        }
        catch (Exception ex) {
            System.out.println("ERROR :"+ex.getMessage());
            if (this.transaction!=null){
                this.transaction.rollback();
            }
            messageFatal("Error Fatal: Por favor contacte con su administrador"+ex.getMessage());
            
        }
            finally
            {
                if (this.session!=null){
                    this.session.close();
                }
            }
            return "/MANTENIMIENTO/FrmManttoUsuario";
    }
    
    
    
    public void cargarCombos(){
        this.session=null;
        this.transaction=null;
        
        try {
            
            this.session = HibernateUtil.getSessionFactory().openSession();
            this.transaction = this.session.beginTransaction();
        //CARGAR COMBOS
            idperfusu=this.musu.getPerfilusario().getIntcodperfilusu();
            idperso=this.musu.getProfesor().getIntcodprofesor();
            varnomusuario=this.musu.getVarnomusuario();
            char op=this.musu.getChrestado();
            if (op=='A') {
                chkestado=true;
            }else{
                chkestado=false;
            }
            insert = Boolean.FALSE;
            
            }
            catch (Exception ex) {
                System.out.println("ERROR :"+ex.getMessage());
                if (this.transaction!=null){
                    this.transaction.rollback();
                }
                messageFatal("Error Fatal: Por favor contacte con su administrador"+ex.getMessage());

            }
                finally
                {
                    if (this.session!=null){
                        this.session.close();
                    }
                }
        }
    
    
    public String cargarNombPersonalxId(int id){
        
        this.session=null;
        this.transaction=null;
        try {
            this.session = HibernateUtil.getSessionFactory().openSession();
            this.transaction = session.beginTransaction();              
            
            this.musu= usudao.ListadoUsuarioxId(this.session,id);
            
//            String ApePate=this.musu.getMpersonal().getVarapellipate();
//            String ApeMate=this.musu.getMpersonal().getVarapellimate();
//            String Nomb=this.musu.getMpersonal().getVarnombre();
                String nomcompleto=null;
//            String nomcompleto=ApePate+", "+ApeMate+" "+Nomb;
//            personaldao.ListadoOficinaxId(session, idoficina);
//            nomoficina=this.personal.getMoficina().getVarnombreoficina();
//            
//            this.transaction.commit();
            return nomcompleto;
        } catch (Exception e) {
//            System.out.println("ERROR :"+e.getMessage());
            if (this.transaction!=null){
                
                transaction.rollback();
            }
            return null;
        }finally{
            if (this.session!=null){
                this.session.close();
            }
        }
            
    }
    public String cargarNombPerfilusuxId(int id){
        
        this.session=null;
        this.transaction=null;
        try {
            this.session = HibernateUtil.getSessionFactory().openSession();
            this.transaction = session.beginTransaction();              
            
            this.musu= usudao.ListadoUsuarioxId(this.session,id);
            
            String nomperfilusu=this.musu.getPerfilusario().getVarnomperfil();
//            personaldao.ListadoOficinaxId(session, idoficina);
//            nomoficina=this.personal.getMoficina().getVarnombreoficina();
//            
//            this.transaction.commit();
            return nomperfilusu;
        } catch (Exception e) {
//            System.out.println("ERROR :"+e.getMessage());
            if (this.transaction!=null){
                
                transaction.rollback();
            }
            return null;
        }finally{
            if (this.session!=null){
                this.session.close();
            }
        }
            
    }
    
    public String salirSistema(){
        this.varnomusuario=null;
        this.varclaveusuario=null;
        
        HttpSession httpSession=(HttpSession) FacesContext.getCurrentInstance().getExternalContext().getSession(true);
        httpSession.invalidate();
//        this.session.close();
        
        return "/index";
    }
    
    public String ingresoSistema(){
        this.session=null;
        this.transaction=null;
        try {
            
            
            this.session = HibernateUtil.getSessionFactory().openSession();
            this.transaction = this.session.beginTransaction();  
            String varnomusuarioC=this.varnomusuarioL.toUpperCase();
            Usuario tUsuario=usudao.validarUsuxVarnomusu(this.session, varnomusuarioC);
//            return "Login/InicioSistemaAdmin";
            if (tUsuario!=null) 
            {
                System.out.println("EXISTE");
                String claveencript=Encryptar.sha5256(varclaveusuarioL);
                if (tUsuario.getVarclaveusuario().equals(claveencript)) {
                    
                    
                    String snomusuario=tUsuario.getVarnomusuario();
                    String snomperfil=tUsuario.getPerfilusario().getVarnomperfil();
                    int coperso=tUsuario.getProfesor().getIntcodprofesor();
                    varGlobal.setNomusuario(snomusuario);
                    varGlobal.setNomperfil(snomperfil);
                    varGlobal.setPubCodPersonal(coperso);
//                    String nomperfil=tUsuario.getMperfilusuario().getVarnomperfil();
                    switch (snomperfil) {
                        case "Administrador":
                            return "Login/InicioSistemaAdmin";
                        case "Profesor":
                            return "Login/InicioSistemaProfe";
                        case "Estudiante":
                            return "Login/InicioSistemaEstudiante";
                    }
                    
                }else{
                    messageError("Usuario o contraseña incorrecto");
                    this.varnomusuario=null;
                    this.varclaveusuario=null;
             
                return "/index";
                }
                
            }
            
            this.transaction.commit();
             
             
        } catch (Exception e) {
            if (this.transaction!=null){
            this.transaction.rollback();
            }
        }finally{
            if (this.session!=null){
                this.session.close();
            }
        }
        messageInfo("No existe el Usuario");
        return "/index";
    }
    
    
    
    public Boolean getInsert() {
        return insert;
    }
    
    public void setInsert(Boolean insert) {
        this.insert = insert;
    }

    public List<Usuario> getListaUsuFiltrado() {
        return listaUsuFiltrado;
    }

    public void setListaUsuFiltrado(List<Usuario> listaUsuFiltrado) {
        this.listaUsuFiltrado = listaUsuFiltrado;
    }

    
    
    
    public boolean isChkestado() {
        return chkestado;
    }

    public void setChkestado(boolean chkestado) {
        this.chkestado = chkestado;
    }

    public List<Profesor> getListPerso() {
        return listPerso;
    }

    public void setListPerso(List<Profesor> listPerso) {
        this.listPerso = listPerso;
    }

    

    

    public int getIdperfusu() {
        return idperfusu;
    }

    public void setIdperfusu(int idperfusu) {
        this.idperfusu = idperfusu;
    }

    

    public int getIdperso() {
        return idperso;
    }

    public void setIdperso(int idperso) {
        this.idperso = idperso;
    }

    public String getVarnomusuario() {
        return varnomusuario;
    }

    public void setVarnomusuario(String varnomusuario) {
        this.varnomusuario = varnomusuario;
    }

    public String getVarclaveusuario() {
        return varclaveusuario;
    }

    public void setVarclaveusuario(String varclaveusuario) {
        this.varclaveusuario = varclaveusuario;
    }

    public String getVarnomusuarioL() {
        return varnomusuarioL;
    }

    public void setVarnomusuarioL(String varnomusuarioL) {
        this.varnomusuarioL = varnomusuarioL;
    }

    public String getVarclaveusuarioL() {
        return varclaveusuarioL;
    }

    public void setVarclaveusuarioL(String varclaveusuarioL) {
        this.varclaveusuarioL = varclaveusuarioL;
    }

    
    public String getVarGNomusu() {
        return varGNomusu;
    }

    public void setVarGNomusu(String varGNomusu) {
        this.varGNomusu = varGNomusu;
    }

    public String getVarGNomperfil() {
        return varGNomperfil;
    }

    public void setVarGNomperfil(String varGNomperfil) {
        this.varGNomperfil = varGNomperfil;
    }

    
    
    
    
    }



    

    

